def reportFileLocation = 'report.html'

def reportResult(reportFile) {
  publishHTML([
    allowMissing: false,
    alwaysLinkToLastBuild: false,
    keepAll: false,
    reportDir: '',
    reportFiles: reportFile,
    reportName: 'SSH Report'
  ])
}

node {
  stage("Clone") {
    git "https://gitlab.com/TLHU/Testsuite.git"
  }

  stage("Test") {
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "ssh", usernameVariable: 'SSH_USER', passwordVariable: 'SSH_PW']]) {
      withEnv() {'SSH_HOST=localHost'}
        try {
          sh "pytest tests/ssh_test.py --html=${reportFileLocation}"
        } catch(err) {
          reportResult(reportFileLocation)
          error "${err}"
        }
      }
    }
  }

  stage("Report") {
    reportResult(reportFileLocation)
  }
}